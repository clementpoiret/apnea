# Apnea

____
## Libraries and required softwares
Libraries used:

- readr,

- xlsx,

- lubridate,

- tidyverse,

- MALDIquant.


According to the nature of the xlsx package, Java Runtime Environment is required and should be linked to a ```JAVA_HOME``` environment variable.

## License
The project is licensed under GNU GPLv3.

____
Copyright 2019 Clément POIRET.