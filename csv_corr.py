# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:48:35 2019

@author: POIRET Clement
"""
# %% IMPORTS
import os
import pandas as pd

#%% FUNCTION
DEFAULT_HEADER = "Name;Time;Start;Stop;Team;Day;Player\n"
BAD_EXPRESSIONS  = ["CATEGORY", "Name;Time;Start;Stop;Team;Player", "Line"]

def isBad(inputfile):
    with open(inputfile) as csv:
        if BAD_EXPRESSIONS[0] in csv.read():
            return True
    return False

def getListOfFiles(dirName):
    # create a list of file and sub directories 
    # names in the given directory 
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)
                
    return allFiles

def correct_csv(inputfile):
    with open(inputfile) as oldcsv, open(inputfile+"_2", 'w') as newcsv:
        newcsv.write(DEFAULT_HEADER)
        
        for line in oldcsv:
            if (not any(bad_exp in line for bad_exp in BAD_EXPRESSIONS)) & (not line.isspace()):
                newcsv.write(line)
                
    os.remove(inputfile)
    os.rename(inputfile+"_2", inputfile)
    
    return pd.read_csv(inputfile, sep=";")
# %% NEED TO BE REFACTORED
print("Please enter the path to a folder or the csv: ")
# Example: E:/Documents/Stages/Fred/Apnea/Data/verticalblue_cnf_william_trubridge_2.csv
inputfile = input()

if ".csv" in inputfile:
    if isBad(inputfile):
        print("Processing ", inputfile)
        informations = inputfile.split("_")
        name = (informations[2] + " " + informations[3]).title()
        
        df = correct_csv(inputfile)
        
        df["Athlete"] = name
        df["Depth_1"] = ""
        df["Depth_2"] = ""
        df["Day"] = informations[-1][:-4]
        
        cols = ["Athlete", "Name", "Start", "Stop", "Depth_1", "Depth_2", "Day"]
        df = df[cols]
        df.to_csv(inputfile, index=False)
    else:
        print("Skipped ", inputfile)
else:
    listOfFiles = getListOfFiles(inputfile)
    sub = ".csv"
    listOfCsv = [s for s in listOfFiles if sub in s]
    
    for csv in listOfCsv:
       
        if not isBad(csv):
            print("Skipped ", csv)
            continue
        
        print("Processed ", csv)
        informations = csv.split("_")
        name = (informations[2] + " " + informations[3]).title()
        
        df = correct_csv(csv)
        
        df["Athlete"] = name
        df["Depth_1"] = ""
        df["Depth_2"] = ""
        df["Day"] = informations[-1][:-4]
        
        cols = ["Athlete", "Name", "Start", "Stop", "Depth_1", "Depth_2", "Day"]
        df = df[cols]
        df.to_csv(csv, index=False)